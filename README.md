Patch ASUS GA503Q (and possibly other) ACPI tables to enable S3 sleep

Please see [this repo](https://gitlab.com/smbruce/GA503QR-StorageD3Enable-DSDT-Patch) for instructions.

Based on files from these repos:
- https://gitlab.com/marcaux/g14-2021-s3-dsdt
- https://gitlab.com/smbruce/GA503QR-StorageD3Enable-DSDT-Patch
